/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.LinkedList;

/**
 *
 * @author Jhon
 */
public class ControlSubida {

    private Bus buses;
    private int contadorBuses;
    private LinkedList<Bus> rutas;

    private Persona pasajeros;
    private int contadorPersonas;
    private LinkedList<Persona> fila;

    public ControlSubida() {
        buses = new Bus();
        rutas = new LinkedList();
        pasajeros = new Persona();
        fila = new LinkedList();
        contadorPersonas = 0;
        contadorBuses = 0;
    }

    public LinkedList<Bus> getRutas() {
        return rutas;
    }

    public void setRutas(LinkedList<Bus> rutas) {
        this.rutas = rutas;
    }

    public LinkedList<Persona> getFila() {
        return fila;
    }

    public void setFila(LinkedList<Persona> fila) {
        this.fila = fila;
    }

    public LinkedList<Persona> ingresoFila() {
        switch (contadorPersonas) {
            case 0:
                pasajeros = new Persona("Pepe", "123");
                break;
            case 1:
                pasajeros = new Persona("Susu", "456");
                break;
            case 2:
                pasajeros = new Persona("Vivi", "789");
                break;
            case 3:
                pasajeros = new Persona("Lulu", "987");
                break;
            case 4:
                pasajeros = new Persona("Jiji", "654");
                break;
            case 5:
                pasajeros = new Persona("Momo", "321");
                break;
        }
        fila.add(pasajeros);
        if (contadorPersonas > 4) {
            contadorPersonas = 0;
        } else {
            contadorPersonas++;
        }
        return fila;
    }

    public LinkedList<Bus> crearRutas(int cupo) {
        switch (contadorBuses) {
            case 0:
                buses = new Bus("YZ0" + contadorBuses, cupo, new LinkedList<Persona>());
                break;
            case 1:
                buses = new Bus("XB0" + contadorBuses, cupo, new LinkedList<Persona>());
                break;
            case 2:
                buses = new Bus("YA0" + contadorBuses, cupo, new LinkedList<Persona>());
                break;
        }
        rutas.add(buses);
        if (contadorBuses > 1) {
            contadorBuses = 0;
        } else {
            contadorBuses++;
        }
        return rutas;
    }
}
