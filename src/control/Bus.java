/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.util.LinkedList;

/**
 *
 * @author Jhon
 */
public class Bus {

    private String placa;
    private int cupo;
    private LinkedList<Persona> pasajeros;

    public Bus() {
        pasajeros = new LinkedList();
    }

    public Bus(String placa, int cupo, LinkedList<Persona> pasajeros) {
        this.placa = placa;
        this.cupo = cupo;
        this.pasajeros = pasajeros;
    }

    public Bus(String placa, int cupo) {
        this.placa = placa;
        this.cupo = cupo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getCupo() {
        return cupo;
    }

    public void setCupo(int cupo) {
        this.cupo = cupo;
    }

    public LinkedList<Persona> getPasajeros() {
        return pasajeros;
    }

    public void setPasajeros(Persona pasajeros) {
        this.pasajeros.add(pasajeros);
    }
}
